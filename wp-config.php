<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm`0M =p{W`5xw| FWrkJ]?}M8D3u><~FNU1IjYOcYQ_9SC#VwOjh)O.c4LM.oOOd');
define('SECURE_AUTH_KEY',  'l.=&#u9Buxe!(HcjxE_c6WVpe@5.lx)G#B`~w2TKV$2-//_QCj+hLd~/h<F6|_$>');
define('LOGGED_IN_KEY',    '7}NR0i&E}a.@9lLo]I^DKz+e.CBzGO(SWL!GkCV`[^y1J67Rzf>#` 8hR*yPX3oc');
define('NONCE_KEY',        'yKcKZUZbv$ppmWpqC78:Iynsad;MpRm{=|uDnx7HY5O6~-[`?r.B&M:w2V>&&R`l');
define('AUTH_SALT',        ',;Gjx$X9myd=O-3`zgnAU^6pEJF?u7Xrw*p(Y>>OmIugF@VB[ <n8X4Hc?w)zRWa');
define('SECURE_AUTH_SALT', '&HiwK:8EG[Rc*kByeqf+_M`wn>dYHCQu 7ZYWIr+[&d]~nkR!iV)RfY0[^[+&H3A');
define('LOGGED_IN_SALT',   '$P,aIC1)yuRPF/& 2D7Uf^HEI,p!D9WHNZ8k=O`+vSN&clJ4D;}5iuHeIdC]t&NT');
define('NONCE_SALT',       'o-]m8fjMLQN!;*;$6l=[);q{n}4rZ!-nMWA)REL;x% ?$g@;ck#v:`[F@FPHnhB=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
