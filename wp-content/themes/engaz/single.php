<?php
 /*
  * Template Name:single_article
  */
get_header(); ?>

	<main role="main">
	<!-- section -->
	<section>
      <div class="overlay">
        <h2 class="title-text"> <?php pll_e('Single Article')?></h2>
        <span><a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home')?> </a> <a
             href="<?=get_the_permalink(pll_get_post(630)) ?>"> <?php pll_e('Articles ')?> </a><?php pll_e('Single Article')?>  </span>
        </div>
    </section>
        <div class="article-single-page ">
        <div class="container">
	   <?php if (have_posts()): while (have_posts()) : the_post(); ?>
           <h3 class="title"><?=the_title(); ?></h3>
            <?php $articles=get_field('article') ?>
           <span><?php pll_e('Add By') ?> <?=$articles['add_by'] ?> </span>
           <span class="date"><?=$articles['date'] ?></span>
           <img src="<?=$articles['img'] ?>">
            <p><?=$articles['first_content'] ?></p>
           <h4><?=$articles['first_title'] ?>  </h4>
           <p><?=$articles['second_content'] ?></p>
           <h4><?=$articles['second_title'] ?> </h4>
           <p><?= $articles['third_content']?></p>
           <i class="fa  fa-comments"><?php pll_e('Add Comments')?> </i>
           <ul class="list-inline">
               <span class="fa fa-share-alt"></span>
               <?php $social_media=get_field('social_media');
               foreach ($social_media as $media):?>
               <li>
             <a href="<?= $media['link_social_media']?>" class="<?=$media['social_media_class'] ?>"></a>
               </li>
            <?php endforeach; ?>
           </ul>
           <div class="comment" id="replay">

             <?php if(comments_open() ):
               comments_template();
             endif;
             ?>
           </div>
           <div class="comment-area">
               <div class="img-user col-xs-12 col-sm-2">
                   <img src="img/user.png">
               </div>
               <div class="past-comment col-xs-12 col-sm-10">

                   <h3> الإسم : المستخدم</h3>
                   <h6 class="fa  fa-clock-o">8:20 م 18/1/2018<a href="#replay"> رد</a></h6>
                   <p>
                       كانت هذه أهم وأفضل المواقع والمنصات التي يمكن الحصول منها على خدمات ترجمة بشرية احترافية للغة العربية واللغات الأخرى، هنالك العديد من النصات الأخرى أو المواقع العادية التي يمكنك الإطلاع عليها بنفسك إن أحببت ذلك، منها:
                   </p>

               </div>
           </div>

           <!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				</a>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<!-- post title -->
			<h1>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</h1>
			<!-- /post title -->

			<!-- post details -->
			<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
			<span class="author"><?php _e( 'Published by', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>
			<span class="comments"><?php if (comments_open( get_the_ID() ) )
			    comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span>
			<!-- /post details -->

			<?php the_content(); // Dynamic Content ?>

			<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

			<p><?php _e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>

			<p><?php _e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>

			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

			<?php comments_template(); ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>
        </div>
        </div>

	<!-- /section -->
	</main>



<?php get_footer(); ?>
