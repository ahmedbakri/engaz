<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=get_template_directory_uri()?>/img/logo1.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=get_template_directory_uri()?>//ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="description" content="<?php bloginfo('description'); ?>">
    <!--Required Files-->
    <link href="https://fonts.googleapis.com/css?family=Teko" rel="stylesheet">
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/slick.css" />
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/bootstrap-rtl.css" />
    <?php if ( pll_current_language()=='ar'): ?>
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/them-rtl.css">
	<?php else:?>
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/them.css" />
    <?php endif?>
	<?php wp_head(); ?>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Start Header-->

    <!--=1= Start Section Of Navigation-->
    <section class="navigation">
        <div class="container">
         
         <?php
         $args=[
           'dropdown'=>1,
           'hide_if_empty'=>1,

         ];
         $translations=pll_the_languages($args);

          ?>

            <ul class="social-icon list-inline">
                <?php $social_media= get_field('social_media',"options") ?>
                <li>
                    <a href="#" class="<?=$social_media['facebook'] ?>"></a>
                </li>
                <li>
                    <a href="#" class="<?=$social_media['twitter'] ?>"></a>
                </li>
                <li>
                    <a href="#" class="<?=$social_media['linked_in'] ?>"></a>
                </li>
                <li>
                    <a href="#" class="<?= $social_media['google']?>"></a>
                </li>
                <li>
                    <a href="#" class="<?= $social_media['you_tube']?>"></a>
                </li>
            </ul>
        </div>
    </section>
    <!--=1= End Section Of Navigation-->


    <!--=2= Strat Section Of Sub Navigation-->
    <section class="sub-navigation">
        <div class="container">
            <a href="<?php echo home_url(); ?>" class="logo">
                <img src="<?=get_template_directory_uri()?>/img/logo1.png">
            </a>

            <div class="call ">
                <i class="fa fa-phone"></i>
                <ul class="list-unstyled">
                 <?php $contact= get_field('contact_us','options') ?>
                    <li><?= $contact['phone1'] ?> </li>
                    <li> <?= $contact['phone2'] ?></li>
                </ul>
            </div>

            <div class="email ">
                <i class="fa fa-envelope"></i>
                <ul class="list-unstyled">
                    <li><?= $contact['email1'] ?></li>
                    <li><?= $contact['email2']?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--=2= End Section Of Sub Navigation-->


    <!--Start Section of Navbar-->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-button" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="request-services " href="<?=get_the_permalink(pll_get_post(397)) ?>">  <?php pll_e('Request Service Now') ?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse special-navbar" id="navbar-button">
                <?php
                   wp_nav_menu(array(
                       'theme_location'=>'primary',
                       'container'    =>false,
                       'menu_class'=>'nav navbar-nav'
                   ));
                   ?>
            </div>
            <!-- /.navbar-collapse -->

        </div>
        <!-- /.container-fluid -->
    </nav>
    <!--End Section of Navbar-->
