<?php
/*
Template Name:request_service
*/
 get_header();
?>
<!-- Start Section of Head-->
<section data-src="img/bg.png" class="head">
    <div class="overlay">
        <h2 class="title-text"> <?php pll_e('Request Service') ?> </h2>
        <span> <a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <?php pll_e('Translating and verifying scientific research') ?> </span>
    </div>
</section>
<!-- End Section of Head-->

<!-- Start Servieces Request Page -->
<section class="serviece-request">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-9">
                <?php
                if (pll_current_language()=='ar'){
                    echo do_shortcode('[hf_form slug="%d8%b7%d9%84%d8%a8-%d8%a7%d9%84%d8%ae%d8%af%d9%85%d8%a9"]');
                }else{
                    echo do_shortcode('[hf_form slug="request-service"]');
                }
                ?>
            </div>

        </div>
    </div>
</section>
<!-- Start Servieces Request Page -->
<?php get_footer(); ?>