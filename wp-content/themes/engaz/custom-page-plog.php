<?php
/* Template Name: plog */

get_header();?>

    <!-- Start Section of Head-->
    <section data-src="img/bg.png" class="head">
        <div class="overlay">
            <h2 class="title-text"> المدونة</h2>
            <span> <a href="index.html"> الرئيسية </a>المدونة</span>
        </div>
    </section>
    <!-- End Section of Head-->

    <div class="blog-page text-center">
    <?php $start_plogs=get_field('startplogs')?>
        <div class="container">
            <div class="row">
             <?php foreach($start_plogs as $plogs):?>
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="article-block">
                        <a href="#" class="img-block-article" data-src="<?=get_template_directory_uri()?>/img/article1.png"></a>
                        <h3><?=$plogs['title']?></h3>
                        <p><?= $plogs['contact']?></p>
                        <hr>
                        <span class="fa fa-comments">20</span>
                        <a href="#">قراءة المزيد</a>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>

        <ul class="list-inline pagenation">
            <li><a href="#">Next</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">Prev</a></li>
        </ul>
    </div>

<?php get_footer(); ?>