<?php
/*Template Name:Jobs */
get_header();

?>

    <!-- Start Section of Head-->
    <section data-src="<?=get_template_directory_uri()?>/img/bg.png" class="head">
        <div class="overlay">
            <h2 class="title-text"><?php pll_e('Request Service') ?> </h2>
            <span> <a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <?php pll_e('Translating and verifying scientific research') ?> </span>
        </div>
    </section>
    <!-- End Section of Head-->

    <!-- Start Servieces Request Page -->
    <section class="serviece-request">

        <div class="container">
            <?php $main_title=get_field('main_title')?>
            <h3><?= $main_title['title']?></h3>
            <p><?= $main_title['contact']?></p>
            <div class="row">
                <div class="col-sm-12 col-md-9 col-lg-9">
                 <?php
                if (pll_current_language()=='ar'){
                echo do_shortcode('[hf_form slug="%d9%88%d8%b8%d8%a7%d8%a6%d9%81"]');
                }else{
                 echo do_shortcode('[hf_form slug="jobs"]');
                }
                ?>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3 ">
                <?php $jobs=get_field('jobs') ?>
                    <div class="category-job">
                        <h5><?= $jobs['title'] ?></h5>
                        <ul class="list-unstyled">
                        <?php foreach($jobs['jobtype'] as $jobtype): ?>
                            <li><a href="#"><?= $jobtype['type']?> </a></li>
                           <?php endforeach?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer() ?>
