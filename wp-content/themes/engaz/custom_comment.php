<?php
/*
 * Template Name:single_article
 */
?>
<div class="comments">
    <?php if (post_password_required()) : ?>
    <p><?php _e( 'Post is password protected. Enter the password to view any comments.', 'html5blank' ); ?></p>
</div>

<?php return; endif; ?>

<form id="comments" class="row form-style">
    <?php if (have_comments()):
        // We Have Comments
        ?>
        <?php
        if (!comments_open()&&get_comments_number()):
            ?>
            <p class="no-comments"><?php esc_html_e('Comments are closed','html5blank')?></p>
        <?php endif;?>
    <?php endif;?>
    <?php comment_form(); ?>
</form>
