<?php
/*
Template Name:Services
*/
get_header();
?>
<div class="services-category-page text-center">

    <div class="container">
        <div class="row">
            <?php $services= new wp_Query(array('post_type'=>'services'));
            foreach ($services ->posts as $service): ?>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="block-services">
                <div class="img-block">
                    <img src="<?=get_template_directory_uri()?>/img/icon1.png">
                </div>
                <h3><?=get_the_title($service->ID); ?></h3>
                <p> <?=get_field('description',$service->ID) ?></p>
                <a href="<?=get_the_permalink($service->ID) ?>"> <?=pll__('Read More') ?></a>
            </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <ul class="list-inline pagenation">
        <li><?php previous_posts_link()?></li>
        <li><?php next_posts_link() ?></li>
    </ul>

</div>
<?php get_footer() ?>
