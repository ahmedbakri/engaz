<?php
  /* Template Name:Quality */
  get_header();
?>

    <!-- Start Section of Head-->
    <section data-src="<?=get_template_directory_uri()?>/img/bg.png" class="head">
        <div class="overlay">
            <h2 class="title-text"> <?php pll_e('Translating and verifying scientific research') ?> </h2>
            <span><a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <a href="<?=get_the_permalink(pll_get_post(374)) ?>"> <?php pll_e('Our service') ?> </a>  <?php  pll_e('Translating and verifying scientific research')?>  </span>
        </div>
    </section>
    <!-- End Section of Head-->

    <!--Start Page Quality-->
    <div class="page-quality">
        <div class="container">
            <div class="quality-img">
                <img src="<?=get_template_directory_uri()?>/img/quality-bg.png">
         </div>
         <?php $translator = get_field("translator"); ?>
            <p> <?= $translator['main_title']?> </p>
            
            <h3>
            <?= $translator['title']?>
            </h3>
            <ul>
              <?php foreach($translator['skills'] as $skill):?>
                <li><?= $skill['skill']?></li>
               
                 <?php endforeach ?>
            </ul>

            <div class="our-certificat">
            <?php $certification = get_field("certification"); ?>
                <h3 class="title-text"><?= $certification['title']?></h3>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="block">

                            <div class="img"><img src="<?= $certification['img']?>"></div>
                            <h4><?=$certification['contact'] ?></h4>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!--End Page Quality-->

   <?php get_footer(); ?>