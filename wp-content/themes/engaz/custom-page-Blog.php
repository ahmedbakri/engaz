<?php
/* Template Name: Blog */

get_header();?>

    <!-- Start Section of Head-->
    <section data-src="<?=get_template_directory_uri()?>/ img/bg.png" class="head">
        <div class="overlay">
            <h2 class="title-text"> <?php pll_e('Blog') ?></h2>
            <span> <a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a><?php pll_e('Blog') ?></span>
        </div>
    </section>
    <!-- End Section of Head-->

    <div class="blog-page text-center">
    <?php $start_blogs=get_field('startblogs')?>
        <div class="container">
            <div class="row">
             <?php foreach($start_blogs as $blogs):?>
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="article-block">
                        <a href="#" class="img-block-article" data-src="<?=$blogs['img']?>"></a>
                        <h3><?=$blogs['title']?></h3>
                        <p><?= $blogs['contact']?></p>
                        <hr>
                        <span class="fa fa-comments">20</span>
                        <a href="#"><?php pll_e('Read More')?></a>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>

        <ul class="list-inline pagenation">
            <li><a href="#">Next</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">Prev</a></li>
        </ul>
    </div>

<?php get_footer(); ?>