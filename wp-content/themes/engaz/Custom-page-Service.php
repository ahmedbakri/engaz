<?php
 /* Template Name: Service */
 get_header()
?>
    
    <section data-src="<?=get_template_directory_uri()?>/img/bg.png" class="head">
        <div class="overlay">
            <h2 class="title-text"><?php pll_e('Translation of scientific researches and materials') ?> </h2>
            <span><a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <a href="<?=get_the_permalink(pll_get_post(374)) ?>"> <?php pll_e('Our Services')?> </a><?php pll_e('Translation of scientific researches and materials')?></span>
        </div>
    </section>
    <!-- End Section of Head-->

    <div class="services-single-page text-center">

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                <?php $service=get_field('custom_service') ?>
                    <h2 class="title-text"><?= $service['title']?></h2>
                   <p><?= $service['contact']?></p>
                    <a href="services-request.html"> <?= $service['request_service']?></a>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="img-services"><img src="<?= $service['img']?>"></div>
                </div>

            </div>
        </div>

    </div>



<?php get_footer() ?>