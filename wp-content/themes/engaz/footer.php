
       <!--Start Section Of Start Project-->
       <section class="start-project">
        <div class="container text-center">
            <h2 class="title-text"> <?php pll_e("Start Your Project Now");?>  </h2>
            <div class="row">
                <div class="col-xs-12  col-sm-3 ">
                    <div class="project">
                        <div class="img-block-project">
                            <img src="<?=get_template_directory_uri()?>/img/project1.png">
                        </div>
                        <h3>  <?php pll_e('choose the language') ?></h3>
                    </div>
                </div>

                <div class="col-xs-12  col-sm-3 ">
                    <div class="project">
                        <div class="img-block-project">
                            <img src="<?=get_template_directory_uri()?>/img/project2.png">
                        </div>
                        <h3> <?php pll_e('Select Specialization') ?> </h3>
                    </div>
                </div>

                <div class="col-xs-12  col-sm-3 ">
                    <div class="project">
                        <div class="img-block-project">
                            <img src="<?=get_template_directory_uri()?>/img/project3.png">
                        </div>
                        <h3><?php pll_e('Attach the file') ?> </h3>
                    </div>
                </div>

                <div class="col-xs-12  col-sm-3 ">
                    <div class="project">
                        <div class="img-block-project">
                            <img src="<?=get_template_directory_uri()?>/img/project4.png">
                        </div>
                        <h3> <?php pll_e('Start project')?></h3>
                    </div>
                </div>
            </div>
            <a href="<?=get_the_permalink(pll_get_post(397)) ?>" class="button-start"><?php pll_e('Start Now') ?></a>
            <a href="<?= get_the_permalink(pll_get_post(210))?>" class="button-contact"> <?php pll_e('Contact Us') ?></a>
        </div>
    </section>
    <!--End Section Of Start Project-->

    <!--Start Section Of Footer-->
    <section class="footer-style">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <a class="logo" href="#"><img src="<?=get_template_directory_uri()?>/img/logo.png"></a>
                   <?php $description=get_field('description','options');
                   $description_en=get_field('description_en','options');
                   ?>
                    <p class="description"><?=(pll_current_language()=='ar')?$description['description_footer']:
                        $description_en['description_footer']?> </p>
                    <a href="<?=(pll_current_language()=='ar')?$description['custom_link']:$description_en['custom_link'] ?>" class="read-more"> <?php pll_e('Read More') ?></a>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <h3><?php pll_e('Important links') ?></h3>

                    <ul class="list-inline hot-linkes">


                            <?php wp_nav_menu(array(
                                'theme_location'=>'footer',
                                'container'    =>false,
                            )); ?>



                    </ul>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <?php
                    $contact_us=get_field('contact_us','options');

                    ?>
                    <h3><?php pll_e('Contact With Us') ?> </h3>
                    <span class="fa fa-envelope"><?=$contact_us['email'] ?></span>
                    <span class="fa fa-phone"><?=$contact_us['phone1'] ?>  </span>
                    <span class="fa fa-map-marker"><?= (pll_current_language()=='ar')?$contact_us['address_details']:
                        get_field('address_details_en','options')?> </span>
                    <h6><?php pll_e('Subscribe to our newsletter so that you can receive all of our news') ?></h6>
                    <input type="text" placeholder="البريد الإلكترونى">
                    <button type="submit"><?php pll_e('Send') ?></button>
                </div>


            </div>
        </div>
    </section>
    <!--End Section Of Footer-->


    <!--Start Section Of copy Right-->
    <section class="copy-right">
        <div class="container">
            <span><?php pll_e('All Rights Reserved To Engaz') ?></span>
            <div class="mahacode-copyrights">
                <a href="#" class="logo"><img src="<?=get_template_directory_uri()?>/img/mahacode.png" alt=""></a>
                <div class="mc-tooltip">
                    <h3> <?php pll_e('Develop & Design To Maha Code company') ?></h3>
                    <h4 class="ti-email">info@mahacode.com</h4>
                    <h4 class="ti-phone">+02686 4621312 14849 8789</h4>
                    <div class="btns-icons">
                        <a href="http://mahacode.com/" class="fa fa-home"></a>
                        <a href="#" class="fa fa-whatsapp"></a>
                        <a href="https://www.behance.net/mahacode" class="fa fa-behance"></a>
                        <a href="https://www.instagram.com/maha.code/" class="fa fa-instagram"></a>
                        <a href="http://www.twitter.com/mahacode" class="fa fa-twitter"></a>
                        <a href="https://www.facebook.com/MahaCode/" class="fa fa-facebook"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Section Of copy Right-->
<?php wp_footer() ?>

    <script src="<?=get_template_directory_uri()?>/js/jquery-3.2.1.min.js"></script>
    <script src="<?=get_template_directory_uri()?>/js/bootstrap.min.js"></script>
       <?php if ( pll_current_language()=='ar'): ?>
           <script src="<?=get_template_directory_uri()?>/js/script-rtl.js"></script>
       <?php else:?>
           <script src="<?=get_template_directory_uri()?>/js/script.js"></script>
       <?php endif?>


    <script src="<?=get_template_directory_uri()?>/js/slick.min.js"></script>
    <script src="<?=get_template_directory_uri()?>/js/jquery.countTo.js"></script>
</body>

</html>