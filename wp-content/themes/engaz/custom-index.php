<?php
 /*
  *  Template Name:Home
  */
 get_header();
?>
<!--Start Slider-->
<div id="header-slider" class="carousel slide" data-ride="carousel">
<?php $custom_plugins=get_field('customize_plugin'); ?>
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach ($custom_plugins as $index => $custom_plugin): ?>
        <li data-target="#header-slider" data-slide-to="<?=$index?>" class="<?= ($index == 1) ? "active" : "" ?>"></li>
        <?php endforeach; ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

         <?php foreach ($custom_plugins as $index => $custom_plugin): ?>
        <div class="item <?= ($index == 1) ? "active" : "" ?>" data-src="<?= $custom_plugin['back_ground_img']?>">
            <!--                <img src="img/bg.png" alt="Bg Of Slider" style="height: 100%; width: 100%">-->
            <div class="carousel-caption">
                <h1> <?=$custom_plugin['title'] ?></h1>
                <p><?=$custom_plugin['content'] ?></p>
                <a href="<?=$custom_plugin['link_contact_us'] ?>" class="button"> <?php pll_e('Read More') ?></a>
                <a href="<?=get_the_permalink(pll_get_post(210))?>" class="button"><?php pll_e("Contact Us") ?></a>
            </div>
        </div>
        <?php endforeach;
        ?>
    </div>

</div>
<!--End Slider-->

<!-- End Header-->
<!--Start Section Of Our Services-->
<section class="our-service">
    <div class="container text-center">
        <h2 class="title-text"><?php pll_e('Services available'); ?></h2>
        <div class="row">
            <?php $services= new wp_Query(array('post_type'=>'services'));
            foreach ($services ->posts as $service): ?>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="block-services">
                    <div class="img-block">
                        <img src="<?=get_template_directory_uri()?>/img/icon1.png">
                    </div>
                    <h3> <?= get_the_title($service->ID)?></h3>
                    <p>  <?=get_field('description',$service->ID) ?></p>
                    <a href="<?=get_the_permalink($service->ID) ?>"> <?php pll_e('Read More') ?></a>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <a href="<?= get_the_permalink(pll_get_post(374))?>" class="button-services"><?php pll_e('Learn about our services'); ?></a>
    </div>
</section>
<!--End Section Of Our Services-->

<!--Start Section Of Video-->
<section class="video text-center" data-src="<?=get_template_directory_uri()?>/img/bg-video.png">
    <div class="overlay">
        <!--         <h2 class="title-text">لماذا نحن</h2>-->
        <h3> <?php pll_e('Learn More About Engaz') ?></h3>
        <span class="fa fa-video-camera" id="myBtn"> </span>
    </div>

</section>

<!-- Trigger/Open The Modal -->

<!-- The Modal -->
<div id="myModal" class="modal">
    <span class="close">&times;</span>
    <!-- Modal content -->
    <div class="modal-content">
        <div class="video-frame">
            <?=get_field('video')?>
        </div>
    </div>

</div>
<!--End Section Of Video-->

<!--Start Section Of Why Choose Us-->
<section class="choose-us">
    <div class="container text-center">
        <h2 class="title-text"><?php pll_e('About Us'); ?></h2>
        <p class="sub-title"> <?php pll_e('We offer many of our services such as')?>:</p>
        <div class="row">
            <?php
            $About_Us=get_field('about_us');
            foreach ($About_Us as $about): ?>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="block-choose">
                    <div class="img-block">
                        <img src="<?=get_template_directory_uri()?>/img/icon1.png">
                    </div>
                    <h3> <?=$about['title'] ?> </h3>
                    <p><?=$about['content'] ?></p>
                </div>
            </div>
             <?php endforeach;?>
        </div>
    </div>
</section>
<!--End Section Of  Why Choose Us-->

<!--Start Section Of Nummbers-->
<section class="nummbers" data-src="<?=get_template_directory_uri()?>/img/bg-video.png">
    <div class="container text-center">
        <h2 class="title-text"> <?php pll_e('Engaz In Numbers') ?></h2>
        <div class="row">
        <?php $numbers=get_field('engaz_in_numbers');
        foreach ($numbers as $number): ?>
            <div class="col-xs-6 col-sm-3 ">
                <div class="block-numbers">
                    <div class="img-block-number">
                        <img src="<?=get_template_directory_uri()?>/img/icon-choose1.png">
                    </div>
                    <h3 class="timer" data-count=<?= $number['custom_number']?>></h3>

                    <h5> <?= $number['custom_name']?></h5>
                </div>
            </div>
          <?php endforeach;?>


        </div>
    </div>
</section>
<!--End Section Of Nummbers-->

<!--Start Section Of Testmonial-->
<section class="testmonial">
    <div class="container text-center">
        <h2 class="title-text"> <?php pll_e('Customer feedback about us')?></h2>
        <div class="row responsive testmoial-slider">
            <?php $testmonea=get_field('customer_feedback_about_us');
                foreach ($testmonea as $test): ?>
                <div class="col-xs-12  col-sm-6">
                    <div class="testmonial-box">
                        <div class="img-block-testmonial">
                            <img src="<?= $test['profile_img']?>">
                        </div>
                        <h3> <?= $test['customer_name']?></h3>
                        <h4><?= $test['job']?> </h4>
                        <p><?= $test['content']?></p>
                    </div>
                </div>
                <?php endforeach; ?>
        </div>
    </div>
</section>
<!--End Section Of Testmonial-->


<!--Start Section Of Article-->
<section class="article">
    <div class="container text-center">
        <h2 class="title-text"><?php pll_e('Follow our latest articles') ?></h2>
        <div class="row">
            <?php $articles= new wp_Query(array('post_type'=>'articles'));
            foreach ($articles ->posts as $article): ?>
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="article-block">
                        <div class="img-block">
                            <?php $art=get_field('article',$article->ID)?>
                        <a href="<?=get_the_permalink(pll_get_post(599)) ?>" class="img-block-article"
                           data-src="<?= $art['img'] ?>"></a>
                        </div>
                        <h3><?=get_the_title($article->ID); ?></h3>
                        <p> <?=get_field('description',$article->ID) ?></p>
                        <a href="<?=get_the_permalink($article->ID) ?>"> <?=pll__('Read More') ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <a href="<?= get_the_permalink(pll_get_post(630))?>" class="artical-button"><?php pll_e('Read More')?></a>
    </div>
</section>
<!--End Section Of Article-->

<!--Start Section of Our Partener-->
<section class="partener">
    <div class="container">
        <h2 class="title-text">بعض عملائنا </h2>
        <div class="row">
         <?php $profiles_img=get_field('customers');

         foreach ($profiles_img as $profiles):?>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="block">
                    <img src="<?=$profiles['customers_profile_img']; ?>">
                </div>
            </div>
           <?php
           endforeach;

           ?>


        </div>
    </div>
</section>
<!--End Section of Our Partener-->



<?php get_footer(); ?>
