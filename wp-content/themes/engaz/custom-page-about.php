<?php
/* Template Name: About Us Page */

get_header();
?>

<!-- Start Section of Head-->
<section data-src="<?=get_template_directory_uri()?>/img/bg.png" class="head">
    <div class="overlay">
    <?php $main_block = get_field("main_block"); ?>
        <h2 class="title-text">  <?=$main_block['title']?>  </h2>
        <span> <a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <?=$main_block['title']?> </span>
    </div>
</section>
<!-- End Section of Head-->

<!--Start Page About Us-->
<section class="about-us text-center">
    <div class="container">
        <h3 class="title-text"><?=$main_block['title']?></h3>
        <span><?=$main_block['description']?></span>
        <p><?=$main_block['content']?></p>
    </div>

    <div class="second-content" data-src="<?=get_template_directory_uri()?>/img/bg-video.png">
        <div class="container">
            <div class="row">
            <?php $blocks = get_field("blocks"); ?>
            <?php foreach($blocks as $block): ?>
                <div class="col-xs-12 col-md-6">
                    <div class="content-block">
                        <h3><i><img src="<?=get_template_directory_uri()?>/img/about-icon2.png"></i> <?=$block['title']?></h3>
                        <p>
                            <?=$block['content'] ?>
                        </p>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>



</section>
<!--End Page About Us-->


<!-- Start Download file -->
<div class="file-download">
    <div class="container">
    <?php $profile = get_field("profile"); ?>
        <h2 class="title-text"><?=$profile['title']?> </h2>
        <p><?= $profile['information']?> :</p>
        <div class="row">
            <div class="col-sm-6 col-md-2">
                <div class="file">
                    <div class="block-img">
                        <img src="<?=get_template_directory_uri()?>/img/pdf.png">
                    </div>
                   
                   <a href="<?= $profile['uploadfile']?>">Profile German</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Download file -->


<?php get_footer(); ?>