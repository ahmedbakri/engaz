<?php
/* Template Name: Language */

get_header();?>


    <!-- Start Section of Head-->
    <section data-src="<?=get_template_directory_uri()?>/img/bg.png" class="head">
        <div class="overlay">
            <h2 class="title-text"><?php pll_e('Translating and verifying scientific research') ?> </h2>
            <span><a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <a href="services-category.html"> <?php pll_e('Our service') ?> </a><?php pll_e('Translating and verifying scientific research') ?></span>
        </div>
    </section>
    <!-- End Section of Head-->

    <!--Start Language Available  -->
    <div class="languge-available">
        <div class="container">
            <div class="quality-img">
                <img src="<?=get_template_directory_uri()?>/img/quality-bg.png">
            </div>
            <p>
               <?= get_field('content')?>
            </p>
            <div class="row">
            <?php $language_available1=get_field('language_available1')?>

                <div class="col-sm-12 col-md-3">
                    <ul>
                     <?php foreach($language_available1 as $lang): ?>
                        <li><?= $lang['language1']?></li>
                         
                        <?php endforeach ?>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-3">
                <?php $language_available2=get_field('language_available2')?>

                    
                    <ul>
                     <?php foreach($language_available2 as $lang): ?>
                        <li><?= $lang['language2']?></li>
                         
                        <?php endforeach ?>

                    </ul>
                </div>
                <div class="col-sm-12 col-md-3">
                <?php $language_available3=get_field('language_available3')?>

                        <ul>
                        <?php foreach($language_available3 as $lang): ?>
                            <li><?= $lang['language3']?></li>
                            
                            <?php endforeach ?>

                        </ul>
                        </div>
                <div class="col-sm-12 col-md-3">
                <?php $language_available4=get_field('language_available4')?>
                    
                    <ul>
                    <?php foreach($language_available4 as $lang): ?>
                        <li><?= $lang['language4']?></li>
                        
                        <?php endforeach ?>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--End Language Available -->

   <?php get_footer(); ?>
