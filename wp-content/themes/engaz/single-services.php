<?php
/*
  * Template Name:Single_Service
  */
get_header(); ?>
<section data-src="<?=get_template_directory_uri()?>/img/bg.png" class="head">
    <div class="overlay">
        <h2 class="title-text"><?php pll_e('Translating and verifying scientific research') ?> </h2>
        <span><a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <a href="<?= get_the_permalink(pll_get_post(374))?>"> <?php pll_e('Our service') ?></a><?php pll_e('Translating and verifying scientific research') ?> </span>
    </div>
</section>
<!-- End Section of Head-->
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
      <div class="services-single-page text-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <h2 class="title-text"><?=the_title() ?></h2>
                        <p><?=get_field('content') ?></p>
                        <a href="<?=get_the_permalink(pll_get_post(397)) ?>"> <?php pll_e('Request Service') ?></a>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="img-services"><img src="<?=get_field('img')?>">
                        </div>
                    </div>
                </div>
            </div>

        </div>

			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
			<?php comments_template(); ?>
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
