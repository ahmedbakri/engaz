<?php
add_theme_support( 'post-thumbnails' );
// To Add Class Form At Plugin Html Form
add_filter('hf_form_element_class_attr', function (){
    return "row form-style";
});
//===============================================
// To Register nav-Menu
function set_set_register(){
 register_nav_menu('primary','Header Navigation Menu');
}
// To Add Action In nav-Menu
add_action('after_setup_theme','set_set_register');
/*
     =====================================
              Add  Nav
     =====================================
 */
  function set_nav_footer(){
      register_nav_menu('footer','Footer Links Important');
  }
  add_action('after_setup_theme','set_nav_footer');
/*
              =============================================
              =============================================
 */


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();
    acf_add_options_sub_page('Custom Information');
    acf_add_options_page(array(
        'page_title' 	=> 'Theme_General_Settings',
        'menu_title'	=> 'Theme_Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'parent_slug'   =>  '',
        'position'      =>false,
        'icon_url'      =>false,
        'redirect'		=> false,
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme_Header_Settings',
        'menu_title'	=> 'Custom_Information',
        'parent_slug'	=> 'theme-general-settings',
        'capability'    => 'edit_post',
        'menu_slug'     => 'theme-general-settings',
        'position'      =>false,
        'icon_url'      =>false,
    ));
}
