<?php get_header(); ?>

<div class="services-category-page text-center">
    <div class="container">
        <div class="row">
                    <?php
                    if (have_posts()):
                        while (have_posts()): the_post();
                    ?>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="block-services">
                    <div class="img-block">
                      <img src="<?=get_template_directory_uri()?>/img/icon1.png">
                    </div>
                    <h3> <?=the_title(); ?></h3>
                    <p><?= get_field('description')?></p>
                    <a href="<?=the_permalink(); ?>"> <?=pll__("Read More")?></a>
                    <?php
                    /*
                  =============================================
                     Comment Functions
                  =============================================
                 */
                 if (comments_open()):
                        comments_template();
                 endif;
                    ?>
                </div>
            </div>
                        <?php
                        endwhile;
                    endif;
                    ?>
        </div>
    </div>

    <ul class="list-inline pagenation">
        <li><a href="#">Next</a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">Prev</a></li>
    </ul>
</div>

<?php get_footer(); ?>
