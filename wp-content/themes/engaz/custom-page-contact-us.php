<?php
/* Template Name: Contact-us */

get_header();
?>
    <!-- Start Section of Head-->
    <?php $contact = get_field("title"); ?>

    <section data-src="<?=get_template_directory_uri()?>/img/bg.png" class="head">
        <div class="overlay">
            <h2 class="title-text"><?=$contact?>   </h2>
            <span>
                <a href="<?=get_the_permalink(pll_get_post(552)) ?>"> <?php pll_e('Home') ?> </a> <?=$contact?>  </span>
        </div>
    </section>
    <!-- End Section of Head-->
    <!-- Start Contact Us Page-->
    <section class="contact-us">
        <div class="container">
            <?php $title= get_field('main_title') ?>
            <h2 class="title-text"> <?=$title['title'] ?></h2>
            <?php
             if (pll_current_language() == "ar"){
            echo do_shortcode('[hf_form slug="%d9%83%d9%86-%d8%b9%d9%84%d9%89-%d8%a5%d8%aa%d8%b5%d8%a7%d9%84-%d9%85%d8%b9%d9%86%d8%a7"]');
             }else {
              echo do_shortcode('[hf_form slug="contact-us"]');
             }
            ?>
        </div>
    </section>
    <!-- End Contact Us Page-->
    <!-- Start Get in Tutch -->
    <?php $location = get_field("location"); ?>
    <div class="get-info">
        <div class="container">
            <h2><?=$title['title'] ?></h2>
            <div class="row">
          <?php foreach($location as $locations):?>
                <div class="col-sm-12 col-lg-6">
                    <h3> <?= $locations['title']?> </h3>
                    <ul class="list-contact">
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <span><?=$locations['name']?></span>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <span><?= $locations['email']?></span>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <span><?=$locations['confirmemail']?></span>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <span><?= $locations['phone']?> </span>
                        </li>
                    </ul>
                    <div class="map-section">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1779298.9801678571!2d30.0493157217576!3d29.423145713899515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1459809c6078ad8f%3A0x55cbaaf346a60b54!2sBeni+Suef+Governorate!5e0!3m2!1sen!2seg!4v1514281471447"
                            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
              <?php endforeach ?>
                    <div class="map-section">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1779298.9801678571!2d30.0493157217576!3d29.423145713899515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1459809c6078ad8f%3A0x55cbaaf346a60b54!2sBeni+Suef+Governorate!5e0!3m2!1sen!2seg!4v1514281471447"
                            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <!-- End Get in Tutch -->
    <?php get_footer(); ?>